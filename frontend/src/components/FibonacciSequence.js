import { useState } from "react";

export function fibonacciSequence(x, y, z) {
  let sequence = [0, 1];

  for (let i = 2; i < x; i++) {
    sequence.push(sequence[i - y] + sequence[i - z]);
  }
  return sequence.slice(0, x);
}

function FibonacciSequence() {
  const [x, setX] = useState(0);
  const [y, setY] = useState(1);
  const [z, setZ] = useState(2);

  const sequence = fibonacciSequence(x, y, z);

  return (
    <div
      className="card card-body text-center"
      style={{ width: "28rem", margin: "10px auto" }}
    >
      <h5 className="card-title">Fibonacci Sequence</h5>
      <h6>
        X:{"   "}
        <input type="number" value={x} onChange={(e) => setX(e.target.value)} />
      </h6>
      <h6>
        Y:{"   "}
        <input type="number" value={y} onChange={(e) => setY(e.target.value)} />
      </h6>
      <h6>
        Z:{"   "}
        <input type="number" value={z} onChange={(e) => setZ(e.target.value)} />
      </h6>
      <div className="card-header">F(x)</div>
      <ul className="list-group list-group-flush">
        {sequence.map((item, index) => (
          <li key={index} className="list-group-item">
            {item}
          </li>
        ))}
      </ul>
    </div>
  );
}

export default FibonacciSequence;
