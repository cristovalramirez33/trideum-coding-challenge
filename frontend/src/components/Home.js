export default function Main() {
  return (
    <div
      className="card-body text-center"
      style={{ width: "38rem", margin: "10px auto" }}
    >
      <h3>FizzBuzz</h3>
      <h6>
        Use the navigation bar to use FizzBuzz and enter an integer as the
        input. The program will then output the appropriate phrase for each
        number from zero to the input integer. You can also specify your own
        divisors and phrases to be used in the program.
      </h6>
      <br />
      <h3>Fibonacci Sequence</h3>
      <h6>
        Fibonacci sequence generates a sequence of numbers based on the
        following formula:
      </h6>
      <h6>F(n) = F(n-1) + F(n-2)</h6>
      <h6>
        where n is the nth number in the sequence. However, this program allows
        you to customize the sequence by inputting values for y and z, resulting
        in the formula:
      </h6>
      <h6>F(x) = F(x-y) + F(x-z) </h6>
      <br />
      <h3>Ultimate FizzBuzz</h3>
      <h6>
        UltimateFizzBuzz is the combination of FizzBuzz and the Fibonacci
        Sequence. It accepts inputs for the divisors for "fizz" and "buzz", as
        well as the words to be displayed instead of "fizz" and "buzz". It also
        accepts inputs for the F(x) sequence, which the follows the formula:
      </h6>
      <h6>F(x) = F(x-y) + F(x-z) </h6>
      <h6>
        If any of these inputs are left blank, default values will be used. The
        program will then display the sequence up to F(x), substituting the
        appropriate words for each F(n) in the sequence according to the
        divisors set.
      </h6>
    </div>
  );
}
