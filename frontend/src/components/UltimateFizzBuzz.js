import { useState } from "react";
import { fibonacciSequence } from "./FibonacciSequence";
function fizzBuzzFibonacci(
  x,
  y,
  z,
  divisor3 = 3,
  divisor5 = 5,
  fizzPhrase = "fizz",
  buzzPhrase = "buzz"
) {
  const fibonacci = fibonacciSequence(x, y, z);
  const output = [];

  for (let i = 0; i < fibonacci.length; i++) {
    if (fibonacci[i] % divisor3 === 0 && fibonacci[i] % divisor5 === 0) {
      output.push(`${fizzPhrase}${buzzPhrase}`);
    } else if (fibonacci[i] % divisor3 === 0) {
      output.push(fizzPhrase);
    } else if (fibonacci[i] % divisor5 === 0) {
      output.push(buzzPhrase);
    } else {
      output.push(fibonacci[i].toString());
    }
  }
  return output;
}

function UltimateFizzBuzz() {
  const [x, setX] = useState(0);
  const [y, setY] = useState(1);
  const [z, setZ] = useState(2);
  const [divisor3, setFizzDivisor] = useState(3);
  const [divisor5, setBuzzDivisor] = useState(5);
  const [fizzPhrase, setFizzPhrase] = useState("fizz");
  const [buzzPhrase, setBuzzPhrase] = useState("buzz");

  const output = fizzBuzzFibonacci(
    x,
    y,
    z,
    divisor3,
    divisor5,
    fizzPhrase,
    buzzPhrase
  );

  return (
    <div
      className="card card-body text-center"
      style={{ width: "28rem", margin: "10px auto" }}
    >
      <h5 className="card-title">Ultimate FizzBuzz</h5>
      <h6>
        X:{"   "}
        <input
          type="number"
          value={x}
          onChange={(e) => setX(parseInt(e.target.value))}
        />
      </h6>
      <h6>
        Y:{"   "}
        <input
          type="number"
          value={y}
          onChange={(e) => setY(parseInt(e.target.value))}
        />
      </h6>
      <h6>
        Z:{"   "}
        <input
          type="number"
          value={z}
          onChange={(e) => setZ(parseInt(e.target.value))}
        />
      </h6>
      <h6>
        1st Divisor:{"   "}
        <input
          type="number"
          value={divisor3}
          onChange={(e) => setFizzDivisor(parseInt(e.target.value))}
        />
      </h6>
      <h6>
        2nd Divisor:{"   "}
        <input
          type="number"
          value={divisor5}
          onChange={(e) => setBuzzDivisor(parseInt(e.target.value))}
        />
      </h6>
      <h6>
        Fizz Phrase:{"   "}
        <input
          type="text"
          value={fizzPhrase}
          onChange={(e) => setFizzPhrase(e.target.value)}
        />
      </h6>
      <h6>
        Buzz Phrase:{"   "}
        <input
          type="text"
          value={buzzPhrase}
          onChange={(e) => setBuzzPhrase(e.target.value)}
        />
      </h6>
      <div className="card-header">F(x)</div>
      <ul className="list-group list-group-flush">
        {output.map((item, index) => (
          <li key={index} className="list-group-item">
            {item}
          </li>
        ))}
      </ul>
    </div>
  );
}

export default UltimateFizzBuzz;
