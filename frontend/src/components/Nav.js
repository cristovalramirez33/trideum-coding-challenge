import Button from "react-bootstrap/Button";
import { useNavigate } from "react-router-dom";

function Nav() {
  const navigate = useNavigate();
  return (
    <>
      <nav className="navbar navbar-expand-sm navbar-dark bg-white">
        <div className="container-fluid">
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-sm-0 ">
              <li className="nav-item">
                <Button
                  className="dropdown-item nav-item"
                  onClick={() => navigate("/")}
                >
                  Home
                </Button>
              </li>
              <li className="nav-item">
                <Button
                  className="dropdown-item nav-item"
                  onClick={() => navigate("/fizzbuzz")}
                >
                  FizzBuzz
                </Button>
              </li>
              <li className="nav-item">
                <Button
                  className="dropdown-item nav-item"
                  onClick={() => navigate("/fibonacci")}
                >
                  Fibonacci
                </Button>
              </li>
              <li className="nav-item">
                <Button
                  className="dropdown-item nav-item"
                  onClick={() => navigate("/fizzbuzz/ultimate")}
                >
                  Ultimate FizzBuzz
                </Button>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
}

export default Nav;
