import { useState } from "react";

function fizzBuzz(
  number,
  divisor3,
  divisor5,
  fizzPhrase = "fizz",
  buzzPhrase = "buzz"
) {
  const output = [];

  for (let i = 1; i <= number; i++) {
    if (i % divisor3 === 0 && i % divisor5 === 0) {
      output.push(`${fizzPhrase}${buzzPhrase}`);
    } else if (i % divisor3 === 0) {
      output.push(fizzPhrase);
    } else if (i % divisor5 === 0) {
      output.push(buzzPhrase);
    } else {
      output.push(i.toString());
    }
  }
  return output;
}

function FizzBuzz() {
  const [number, setN] = useState(0);
  const [divisor3, setDivisor3] = useState(3);
  const [divisor5, setDivisor5] = useState(5);
  const [fizzPhrase, setFizzPhrase] = useState("fizz");
  const [buzzPhrase, setBuzzPhrase] = useState("buzz");

  const output = fizzBuzz(number, divisor3, divisor5, fizzPhrase, buzzPhrase);

  return (
    <div
      className="card card-body text-center"
      style={{ width: "28rem", margin: "10px auto" }}
    >
      <h5 className="card-title">FizzBuzz</h5>
      <h6>
        Enter Number:{"   "}
        <input
          type="number"
          value={number}
          onChange={(e) => setN(e.target.value)}
        />
      </h6>
      <h6>
        1st Divisor:{"   "}
        <input
          type="number"
          value={divisor3}
          onChange={(e) => setDivisor3(e.target.value)}
        />
      </h6>
      <h6>
        2nd Divisor:{"   "}
        <input
          type="number"
          value={divisor5}
          onChange={(e) => setDivisor5(e.target.value)}
        />
      </h6>
      <h6>
        Fizz Phrase:{"   "}
        <input
          type="text"
          value={fizzPhrase}
          onChange={(e) => setFizzPhrase(e.target.value)}
        />
      </h6>
      <h6>
        Buzz Phrase:{"   "}
        <input
          type="text"
          value={buzzPhrase}
          onChange={(e) => setBuzzPhrase(e.target.value)}
        />
      </h6>
      <div className="card-header">Results</div>
      <ul className="list-group list-group-flush">
        {output.map((item, index) => (
          <li key={index} className="list-group-item">
            {item}
          </li>
        ))}
      </ul>
    </div>
  );
}

export default FizzBuzz;
