import { BrowserRouter, Routes, Route } from "react-router-dom";

import Home from "./components/Home";
import Nav from "./components/Nav";
import FizzBuzz from "./components/FizzBuzz";
import FibonacciSequence from "./components/FibonacciSequence";
import UltimateFizzBuzz from "./components/UltimateFizzBuzz";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="fizzbuzz">
          <Route path="" element={<FizzBuzz />} />
          <Route path="ultimate" element={<UltimateFizzBuzz />} />
        </Route>
        <Route path="fibonacci" element={<FibonacciSequence />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
