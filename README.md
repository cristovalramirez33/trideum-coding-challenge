# Ultimate FizzBuzz

## FizzBuzz

FizzBuzz is a program that accepts an integer as input and produces an output for every integer from zero to the input number. The program will display a specified phrase for numbers divisible by a user-specified divisor, and a different specified phrase for numbers divisible by another user-specified divisor. Additionally, the program will display a combination of the two phrases for numbers that are divisible by both of the user-specified divisors.

To use FizzBuzz, simply run the program and enter an integer as the input. The program will then output the appropriate phrase for each number from zero to the input integer. You can also specify your own divisors and phrases to be used in the program.

## Fibonacci Sequence

This program generates a sequence of numbers based on the following formula: F(n) = F(n-1) + F(n-2), where n is the nth number in the sequence. However, this program allows you to customize the sequence by inputting values for y and z, resulting in the formula F(x) = F(x-y) + F(x-z).

If a value for F(x) does not exist yet in the sequence, the program will output a value of 1.

To use the program, simply input values for y and z and the program will generate the modified Fibonacci sequence.

## Ultimate FizzBuzz

UltimateFizzBuzz is the combination of FizzBuzz and the Fibonacci Sequence. It accepts inputs for the divisors for "fizz" and "buzz", as well as the words to be displayed instead of "fizz" and "buzz". It also accepts inputs for the F(x) sequence, where F(x) = F(x-y) + F(x-z). If any of these inputs are left blank, default values will be used. The program will then display the sequence up to F(x), substituting the appropriate words for each F(n) in the sequence according to the divisors set.

## Features

- Responsive design: The website layout adjusts to fit different screen sizes, such as mobile phones and tablets.
- Navigation menu: A menu that allows users to navigate to different pages on the website.

## Set up

1. Clone: https://gitlab.com/cristovalramirez33/ultimate-fizzbuzz
2. run the command:
   - cd frontend
   - npm install
   - npm start
3. Open up web browser and go to: http://localhost:3000/
   Success!

## Dependencies

- Bootstrap
- React
- React Bootstrap
- React Router Dom
